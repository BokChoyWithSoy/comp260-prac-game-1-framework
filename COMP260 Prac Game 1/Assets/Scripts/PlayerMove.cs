using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
public float maxSpeed = 5.0f;
public float acceleration = 3.0f;
public float brake = 5.0f;
public float turnSpeed = 30.0f;
private float speed = 0.0f;

	void Update()
	{
		float forwards = Input.GetAxis("Vertical");
		if(forwards > 0)
		{
			speed = speed + acceleration * Time.deltaTime;
		}
		else if(forwards < 0)
		{
			speed = speed - acceleration * Time.deltaTime;
		}
    else
    {
      if(speed > 0)
      {
        speed = speed * Time.deltaTime;
      }
      else if(speed < 0 )
      {
        speed = speed + brake * Time.deltaTime;
      }
    }

    float turn = Input.GetAxis("Horizontal");

    if(forwards > 0)
    {
      turnSpeed = speed*10;
    }
    else if(forwards < 0)
    {
      turnSpeed = speed*5;
    }


    transform.Rotate(0 , 0 , turn * turnSpeed * Time.deltaTime);

    speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		Vector2 velocity = Vector2.up * speed;

		transform.Translate(velocity * Time.deltaTime);
	}
}
