﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

public float speed = 4.0f;
public float turnSpeed = 180.0f;
public Transform target;
public Transform target2;
private Vector2 heading = Vector2.right;


	void Update ()
	{
		Vector2 direction2 = target2.position - transform.position;
		Vector2 direction = target.position - transform.position;
		float dir = direction.magnitude;
		float dir2 = direction2.magnitude;
		print(dir + "direction1");
		print(dir2 + "direction2");

		float angle = turnSpeed * Time.deltaTime;
		if(dir < dir2)
		{
		if(direction.IsOnLeft(heading))
			{
				heading = heading.Rotate(angle);
			}
			else
			{
				heading = heading.Rotate(-angle);
			}
		}
		else
		{
			if(direction2.IsOnLeft(heading))
				{
					heading = heading.Rotate(angle);
				}
				else
				{
					heading = heading.Rotate(-angle);
				}
		}
		transform.Translate(heading*speed*Time.deltaTime);
		}
}
