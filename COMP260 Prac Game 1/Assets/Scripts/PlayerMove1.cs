﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove1 : MonoBehaviour
{
public float maxSpeed = 5.0f;
	// Update is called once per frame
	void Update()
	{
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal1");
		direction.y = Input.GetAxis("Vertical1");

		Vector2 velocity= direction * maxSpeed;

		transform.Translate(velocity * Time.deltaTime);
	}
}
